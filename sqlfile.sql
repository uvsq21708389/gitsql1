CREATE DATABASE cambridge3;

CREATE TABLE cities (
    name character varying(80),
    location character varying(80)
);

INSERT INTO cities(name,location) values ('San Francisco','USA');
